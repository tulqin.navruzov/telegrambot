package telgramapp.telegrambot.congfig;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import telgramapp.telegrambot.controller.dto.ResponseData;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class ProjectAuthenticationEntryPoint implements AuthenticationEntryPoint {

    @Override
    public void commence(HttpServletRequest request,
                         HttpServletResponse response,
                         AuthenticationException arg2) throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();

        ResponseData responseItem = new ResponseData<>(null, "Session expired", HttpStatus.UNAUTHORIZED.value());
        String json = objectMapper.writeValueAsString(responseItem);

        response.setContentType("application/json; charset=UTF-8");
        PrintWriter printout = response.getWriter();
        printout.print(json);
        printout.flush();
        response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
    }
}
