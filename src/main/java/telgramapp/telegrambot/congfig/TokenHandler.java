package telgramapp.telegrambot.congfig;

import telgramapp.telegrambot.controller.dto.UserAuth;

public interface TokenHandler {

    UserAuth getUser(String token);

    String createToken(UserAuth userAuth);

    void removeUser(String token);

    Long sessionTimeoutMinutes();
}
