package telgramapp.telegrambot.congfig;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;
import telgramapp.telegrambot.db.EdsMember;
import telgramapp.telegrambot.service.MemberService;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Slf4j
@Component
public class ProjectUserDetailsService implements UserDetailsService {

    @PersistenceContext
    private EntityManager entityManager;
    @Autowired
    private MemberService memberService;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        log.debug("Get {} login details from system", username);

        String sql = "select m from EdsMember m " +
                "  where m.login=:username ";

        EdsMember memberDomain;
        try {
            memberDomain = (EdsMember) entityManager.createQuery(sql)
                    .setParameter("username", username)
                    .getSingleResult();
        } catch (Exception e) {
            throw new UsernameNotFoundException("Incorrect credentials");
        }
        return memberService.getUserAuth(memberDomain.getId());
    }
}
