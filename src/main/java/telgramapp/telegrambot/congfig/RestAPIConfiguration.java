package telgramapp.telegrambot.congfig;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.ehcache.EhCacheCacheManager;
import org.springframework.cache.ehcache.EhCacheManagerFactoryBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.authentication.logout.LogoutHandler;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;

import java.util.Objects;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true, securedEnabled = true)
public class RestAPIConfiguration extends WebSecurityConfigurerAdapter {


    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) {
        auth.authenticationProvider(authProvider());
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.headers()
                .frameOptions()
                .deny();

        http.csrf().disable()
                .exceptionHandling()
                .authenticationEntryPoint(this.authenticationEntryPoint())
                .and()
                .formLogin()
                .permitAll()
                .successHandler(this.successHandler())
                .failureHandler(this.failureHandler())
                .and()
                .addFilterBefore(new ProjectCorsFilter(), UsernamePasswordAuthenticationFilter.class)
                .addFilterBefore(this.projectLoginFilter(), UsernamePasswordAuthenticationFilter.class)
                .addFilterAfter(this.projectAuthenticationFilter(), UsernamePasswordAuthenticationFilter.class)
                .sessionManagement()
                .maximumSessions(3); //maximum number of concurrent sessions for one user

        http.authorizeRequests()
                .antMatchers("/api/v1/member/logout").permitAll()
                .antMatchers("/api/v1/queue/public/**").permitAll()
                .antMatchers("/api/**").authenticated();
    }

    @Bean
    @Override
    public ProjectUserDetailsService userDetailsService() {
        return new ProjectUserDetailsService();
    }

    @Bean
    public AuthenticationFilter authenticationFilter() throws Exception {
        AuthenticationFilter filter = new AuthenticationFilter();
        filter.setAuthenticationManager(this.authenticationManager());
        filter.setAuthenticationSuccessHandler(this.successHandler());
        filter.setAuthenticationFailureHandler(this.failureHandler());
        filter.setUsernameParameter("username");
        filter.setPasswordParameter("password");
        return filter;
    }

    @Bean
    public DaoAuthenticationProvider authProvider() {
        DaoAuthenticationProvider provider = new DaoAuthenticationProvider();
        provider.setUserDetailsService(userDetailsService());
        provider.setPasswordEncoder(passwordEncoder());
        provider.setHideUserNotFoundExceptions(false);
        return provider;
    }

    @Bean
    public BCryptPasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    public EhCacheManagerFactoryBean ehCacheManagerFactoryBean() {
        EhCacheManagerFactoryBean ehCache = new EhCacheManagerFactoryBean();
        ehCache.setConfigLocation(new ClassPathResource("ehcache.xml"));
        return ehCache;
    }

    @Bean
    public EhCacheCacheManager getEhCacheCacheManager() {
        return new EhCacheCacheManager(Objects.requireNonNull(ehCacheManagerFactoryBean().getObject()));
    }

    @Bean
    public TokenAuthenticationHandler tokenAuthenticationHandler() {
        return new TokenAuthenticationHandler(this.cacheTokenHandler());
    }

    @Bean
    public CacheTokenHandler cacheTokenHandler() {
        return new CacheTokenHandler();
    }

    @Bean
    public SimpleUrlAuthenticationFailureHandler failureHandler() {
        return new SimpleUrlAuthenticationFailureHandler();
    }

    @Bean
    public ProjectAuthenticationSuccessHandler successHandler() {
        return new ProjectAuthenticationSuccessHandler();
    }

    @Bean
    public ProjectAccessDeniedHandler accessDeniedHandler() {
        return new ProjectAccessDeniedHandler();
    }

    @Bean
    public ProjectAuthenticationEntryPoint authenticationEntryPoint() {
        return new ProjectAuthenticationEntryPoint();
    }

    @Bean
    public ProjectAuthenticationFilter projectAuthenticationFilter() {
        return new ProjectAuthenticationFilter(this.tokenAuthenticationHandler());
    }

    @Bean
    public ProjectLoginFilter projectLoginFilter() throws Exception {
        return new ProjectLoginFilter("/api/v1/member/login",
                this.tokenAuthenticationHandler(),
                this.userDetailsService(),
                this.authenticationManager()
        );
    }

    @Bean
    public LogoutHandler logoutHandler() {
        return new SecurityContextLogoutHandler();
    }
}
