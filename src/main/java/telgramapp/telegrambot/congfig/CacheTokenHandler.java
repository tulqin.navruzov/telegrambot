package telgramapp.telegrambot.congfig;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.util.Assert;
import telgramapp.telegrambot.contants.Constants;
import telgramapp.telegrambot.controller.dto.UserAuth;
import telgramapp.telegrambot.utils.DataGetter;

import javax.xml.bind.DatatypeConverter;
import java.nio.charset.StandardCharsets;
import java.util.UUID;

@Slf4j
public class CacheTokenHandler implements TokenHandler {

    @Autowired
    private CacheManager cacheManager;

    private Cache getCache() {
        return cacheManager.getCache("authTokenCache");
    }

    @Override
    public UserAuth getUser(String token) {
        CacheTokenTO cacheTokenTO = getTokenHolderFromToken(token);
        if (cacheTokenTO == null) {
            return null;
        }
        Cache.ValueWrapper valueWrapper = getCache().get(cacheTokenTO.getToken());

        if (valueWrapper != null) {
            UserAuth userAuth = (UserAuth) valueWrapper.get();
            if (userAuth != null && !token.equals(userAuth.getCurrentToken())) {
                log.warn("Token {} not match with current token {} of user {} ", token, userAuth.getCurrentToken());
            } else {
                return (UserAuth) valueWrapper.get();
            }
        }
        return null;
    }

    @Override
    public String createToken(UserAuth userAuth) {
        Assert.notNull(userAuth, "UserAuth object is null");
        Assert.notNull(userAuth.getUserId(), "User id is null");

        CacheTokenTO cacheTokenTO = this.getTokenHolderFromId(userAuth.getUserId());
        String token = cacheTokenTO.getToken();

        userAuth.setCurrentToken(token);
        getCache().put(cacheTokenTO.getToken(), userAuth);
        return token;
    }

    @Override
    public void removeUser(String token) {
        CacheTokenTO cacheTokenTO = this.getTokenHolderFromToken(token);
        if (cacheTokenTO == null) {
            return;
        }
        getCache().evict(cacheTokenTO.getToken());
    }

    @Override
    public Long sessionTimeoutMinutes() {
        return null;  //todo session time out minutes
    }

    private String normalizeUid(String uid) {
        return uid.toUpperCase().replace('-', '1');
    }

    private CacheTokenTO getTokenHolderFromId(Long userId) {
        String salt1 = this.normalizeUid(UUID.randomUUID().toString());
        String userHashId = DataGetter.getString(userId);
        String salt2 = this.normalizeUid(UUID.randomUUID().toString());
        String rawToken = salt1 + Constants.SEPARATOR + userHashId + Constants.SEPARATOR + salt2;
        String token = DatatypeConverter.printBase64Binary(rawToken.getBytes(StandardCharsets.UTF_8));

        return new CacheTokenTO(salt1, salt2, token);
    }

    private CacheTokenTO getTokenHolderFromToken(String token) {
        String rawToken;
        try {
            rawToken = new String(DatatypeConverter.parseBase64Binary(token), StandardCharsets.UTF_8);
        } catch (Exception e) {
            return null;
        }
        String[] tokens = rawToken.split(Constants.SEPARATOR);

        if (tokens.length != 3) {
            return null;
        }
        return new CacheTokenTO(tokens[0], tokens[2], token);
    }
}

