package telgramapp.telegrambot.congfig;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import telgramapp.telegrambot.controller.dto.UserAuth;

import java.util.Collection;

public class UserAuthentication implements Authentication {

    private final UserAuth user;
    private boolean authenticated = true;

    UserAuthentication(UserAuth user) {
        this.user = user;
    }

    @Override
    public String getName() {
        return user.getFullName();
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return user.getAuthorities();
    }

    @Override
    public Object getCredentials() {
        return user.getPassword();
    }

    @Override
    public UserAuth getDetails() {
        return user;
    }

    @Override
    public Object getPrincipal() {
        return user.getFullName();
    }

    @Override
    public boolean isAuthenticated() {
        return authenticated;
    }

    @Override
    public void setAuthenticated(boolean authenticated) {
        this.authenticated = authenticated;
    }
}
