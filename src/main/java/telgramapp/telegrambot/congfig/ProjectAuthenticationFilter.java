package telgramapp.telegrambot.congfig;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.GenericFilterBean;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class ProjectAuthenticationFilter extends GenericFilterBean {

    private final TokenAuthenticationHandler tokenAuthenticationHandler;

    public ProjectAuthenticationFilter(TokenAuthenticationHandler tokenAuthenticationHandler) {
        this.tokenAuthenticationHandler = tokenAuthenticationHandler;
    }

    @Override
    public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException, ServletException {
        Authentication authentication = tokenAuthenticationHandler.getAuthentication((HttpServletRequest) req, (HttpServletResponse) res);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        chain.doFilter(req, res);
    }
}