package telgramapp.telegrambot.congfig;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import telgramapp.telegrambot.controller.dto.ResponseData;
import telgramapp.telegrambot.controller.dto.UserAuth;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class ProjectLoginFilter extends AbstractAuthenticationProcessingFilter {

    private final TokenAuthenticationHandler tokenAuthenticationHandler;
    private final UserDetailsService userDetailsService;

    public ProjectLoginFilter(String urlMapping,
                              TokenAuthenticationHandler tokenAuthenticationHandler,
                              UserDetailsService userDetailsService,
                              AuthenticationManager authManager) {
        super(new AntPathRequestMatcher(urlMapping));
        this.userDetailsService = userDetailsService;
        this.tokenAuthenticationHandler = tokenAuthenticationHandler;
        setAuthenticationManager(authManager);
    }

    @Override
    public Authentication attemptAuthentication(HttpServletRequest request,
                                                HttpServletResponse response) throws AuthenticationException, IOException {
        UserAuth user = new ObjectMapper().readValue(request.getInputStream(), UserAuth.class);
        UsernamePasswordAuthenticationToken loginToken = new UsernamePasswordAuthenticationToken(user.getUsername(),
                user.getPassword());
        return getAuthenticationManager().authenticate(loginToken);
    }

    @Override
    protected void successfulAuthentication(HttpServletRequest request,
                                            HttpServletResponse response,
                                            FilterChain chain,
                                            Authentication authentication) throws IOException {
        String username = ((UserAuth) authentication.getPrincipal()).getUsername();
        UserAuth authenticatedUser = (UserAuth) userDetailsService.loadUserByUsername(username);
        UserAuthentication userAuthentication = new UserAuthentication(authenticatedUser);
        authenticatedUser.setPassword(null);
        tokenAuthenticationHandler.addAuthentication(response, userAuthentication);

        SecurityContextHolder.getContext().setAuthentication(userAuthentication);

        ObjectMapper objectMapper = new ObjectMapper();
        String json = objectMapper.writeValueAsString(new ResponseData<>(
                new TokenTO(userAuthentication.getDetails().getCurrentToken())
        ));

        response.setContentType("application/json; charset=UTF-8");
        PrintWriter printout = response.getWriter();
        printout.print(json);
        printout.flush();
    }

    @Override
    protected void unsuccessfulAuthentication(HttpServletRequest request,
                                              HttpServletResponse response,
                                              AuthenticationException failed) throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        String json = objectMapper.writeValueAsString(new ResponseData<>(null, "Incorrect Login or Password", HttpStatus.UNAUTHORIZED.value()));

        response.setContentType("application/json; charset=UTF-8");
        PrintWriter printout = response.getWriter();
        printout.print(json);
        printout.flush();
        response.setStatus(HttpStatus.UNAUTHORIZED.value());
    }
}