package telgramapp.telegrambot.congfig;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.io.Serializable;

@Getter
@AllArgsConstructor
class CacheTokenTO implements Serializable {

    private String salt1;
    private String salt2;
    private String token;
}
