package telgramapp.telegrambot.congfig;

import org.springframework.security.core.Authentication;
import telgramapp.telegrambot.contants.Constants;
import telgramapp.telegrambot.controller.dto.UserAuth;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class TokenAuthenticationHandler {

    private Long sessionTimeoutMinutes;
    private TokenHandler tokenHandler;

    public TokenAuthenticationHandler(TokenHandler tokenHandler) {
        this.tokenHandler = tokenHandler;
        if (tokenHandler != null) {
            this.sessionTimeoutMinutes = tokenHandler.sessionTimeoutMinutes();
        }
    }

    void addAuthentication(HttpServletResponse response, UserAuthentication authentication) {
        UserAuth userAuth = authentication.getDetails();
        if (sessionTimeoutMinutes != null) {
            userAuth.setExpires(System.currentTimeMillis() + (1000L * 60L * sessionTimeoutMinutes));
        }
        response.addHeader(Constants.AUTH_HEADER, tokenHandler.createToken(userAuth));
    }

    Authentication getAuthentication(HttpServletRequest request, HttpServletResponse response) {
        final String token = request.getHeader(Constants.AUTH_HEADER);
        if (token == null) {
            return null;
        }
        UserAuth userAuth = tokenHandler.getUser(token);
        if (userAuth == null) {
            return null;
        }
        UserAuthentication userAuthentication = new UserAuthentication(userAuth);
        if (sessionTimeoutMinutes != null) {
            addAuthentication(response, userAuthentication);
        }
        return userAuthentication;
    }

    public void removeAuthentication(HttpServletRequest request) {
        final String token = request.getHeader(Constants.AUTH_HEADER);
        if (token != null) {
            tokenHandler.removeUser(token);
        }
    }
}

