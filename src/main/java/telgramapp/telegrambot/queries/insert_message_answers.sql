delete from telegrammessages;
insert into telegrammessages (messagekey, answertext)
values
('/start','Endi Bizda MEGA KONKURS !

                Tanlovda ishtirok etish uchun oddiy 2 ta shartni bajaring

                ✅ 7 ta kanalga a''zo bo''ling.
                ✅ Postni 5 nafar do''stingizga yuboring'),

('Tanlov shartlari','❗️❗️❗️Diqqat❗️❗️❗️

1. Konkursda barchasi haqqoniy va halol bo''lishiga kafolat beramiz.

2. Har hafta 2 nafar g''olibni berilgan tartib raqamlari bo''yicha randstuff.ru/number sayti orqali tasodifiy tarzda tanlab olamiz. G''oliblar soni – 10 nafar. Ularning har biriga 5 million so''mdan pul mukofoti beriladi.

3. Konkurs haqidagi xabarni kamida 5 nafar do''stingizga yuborishingiz shart. Ular siz yuborgan xabar asosida botga kirib, start tugmasini bosishlari kerak. Agar g''olib bo''lsangiz, barcha shartlarni bajarganingiz tekshiriladi.

4. Konkurs 2019-yil 20-dekabr soat 23:59:59ga qadar davom etadi. A''zo bo''lgan kanallaringizdan shu muddatgacha chiqib ketmasligingiz kerak. Albatta, undan keyin ham, chunki bu – hali boshlanishi. Sizni oldinda yana ko''plab yutuqli tanlovlar kutmoqda.'),

('Ishtirok Etish','✅ 7 ta kanalga a''zo bo''ling.


👉 @kunuzofficial

👉 @imperiyauzbekistan

👉 @xabardoruz

👉 @viqor

👉 @Mebellarmarkazi

👉 @tribunauz

👉 @kundalikuz


✅ A''zo bo''lganingizdan so''ng "BAJARILDI" tugmasini bosing.'),


('Bajarilmadi','Siz kanallarga to''liq a''zo bo''lmadingiz !!!

👉 Yoki telefon raqamingizni bizga junatmadingiz ');