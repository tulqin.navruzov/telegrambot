package telgramapp.telegrambot.botsender;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.extensions.bots.commandbot.TelegramLongPollingCommandBot;
import org.telegram.telegrambots.meta.api.methods.ParseMode;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.methods.send.SendPhoto;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardButton;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardRow;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import telgramapp.telegrambot.contants.Constants;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@Component
public class TelegramBotSender extends TelegramLongPollingCommandBot {

    public void sendMessage(Update update, String answerText, String secondStepMsg, boolean hasPhone) {
        try {
            if (!update.hasMessage()) {
                log.error("Update doesn't have a body!");
                answerText = "Update doesn't have a body!";
            }
            SendMessage answer = new SendMessage(update.getMessage().getChatId(), answerText);


            ReplyKeyboardMarkup keyboard = new ReplyKeyboardMarkup();
            List<KeyboardRow> keyboardList = new ArrayList<>();

            KeyboardRow firstRow = new KeyboardRow();
            firstRow.add(secondStepMsg);
            keyboardList.add(firstRow);

            if (!hasPhone) {
                KeyboardRow thirdRow = new KeyboardRow();
                KeyboardButton shareContact = new KeyboardButton("Telefon raqamini yuborish");
                shareContact.setRequestContact(true);
                thirdRow.add(shareContact);
                keyboardList.add(thirdRow);
            }

            keyboard.setResizeKeyboard(true);
            keyboard.setKeyboard(keyboardList);

            answer.setParseMode(ParseMode.HTML);
            answer.setReplyMarkup(keyboard);
            execute(answer);
        } catch (TelegramApiException e) {
            e.printStackTrace();
        }

    }

    public void sendPhoto(Update update, String answerText, boolean hasPhone) {
        try {
            if (!update.hasMessage()) {
                log.error("Update doesn't have a body!");
                answerText = "Update doesn't have a body!";
            }
            SendPhoto answer = new SendPhoto();
            answer.setChatId(update.getMessage().getChatId());
            answer.setParseMode(ParseMode.HTML);
            answer.setPhoto("https://homepages.cae.wisc.edu/~ece533/images/pool.png");
            answer.setCaption(answerText);


            ReplyKeyboardMarkup keyboard = new ReplyKeyboardMarkup();
            List<KeyboardRow> keyboardList = new ArrayList<>();

            KeyboardRow firstRow = new KeyboardRow();
            firstRow.add("Tanlov shartlari");
            keyboardList.add(firstRow);

            if (!hasPhone) {
                KeyboardRow thirdRow = new KeyboardRow();
                KeyboardButton shareContact = new KeyboardButton("Telefon raqamini yuborish");
                shareContact.setRequestContact(true);
                thirdRow.add(shareContact);
                keyboardList.add(thirdRow);
            }

            keyboard.setResizeKeyboard(true);
            keyboard.setKeyboard(keyboardList);
            answer.setReplyMarkup(keyboard);

            execute(answer);
        } catch (TelegramApiException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void processNonCommandUpdate(Update update) {

    }

    @Override
    public String getBotToken() {
        return Constants.LOCAL_BOT_TOKEN;
    }
}
