package telgramapp.telegrambot.utils;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.apache.commons.lang3.StringUtils;

import java.io.Serializable;

@Getter
@Setter
@ToString(onlyExplicitlyIncluded = true, doNotUseGetters = true)
public class BaseListFilter  implements Serializable {
    @ToString.Include
    private Integer start = 0;
    @ToString.Include
    private Integer limit = 20;
    @ToString.Include
    private String searchKey;
    @ToString.Include
    private String sortField;
    @ToString.Include
    private SortType sortOrder;
    private Long type;
    private Long organizationId;

    public SortType getSortOrder() {
        if (sortOrder == null) {
            return SortType.asc;
        }
        return sortOrder;
    }

    public String getLikeSearchKey() {
        if (StringUtils.isEmpty(this.getSearchKey())) {
            return "";
        }
        return "%" + this.getSearchKey().toLowerCase() + "%";
    }
}
