package telgramapp.telegrambot.utils;

import org.apache.commons.lang3.StringUtils;

import java.text.SimpleDateFormat;
import java.util.Date;

public final class DataGetter {
    private static final String EMPTY = "";
    private static final SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");

    public static long getLong(Number value) {
        return getLong(value, 0L);
    }

    private static long getLong(Number value, Long defValue) {
        return value == null ? getLong(defValue) : value.longValue();
    }

    public static double getDouble(Number value) {
        return getDouble(value, 0d);
    }

    private static double getDouble(Number value, double defaultValue) {
        return value == null ? defaultValue : value.doubleValue();
    }

    public static int getInteger(Number value, int defaultValue) {
        return value == null ? defaultValue : value.intValue();
    }

    public static String getString(Number n) {
        if (n != null && n.intValue() > 0) {
            return String.valueOf(n);
        }
        return EMPTY;
    }

    public static boolean getBoolean(Boolean value) {
        if (value == null) {
            return false;
        }
        return value;
    }

    public static boolean validatePhone(String phone) {
        return phone.matches("^9989[\\d]{8}$");
    }

    public static String getShortTimeFormatted(long milliseconds) {
        return timeFormat.format(new Date(milliseconds));
    }

    public static String createConstant(String text) {
        return StringUtils.isNotEmpty(text) ?
                text.trim().replaceAll("[^a-zA-Z]+", "_").toUpperCase() :
                null;
    }
}
