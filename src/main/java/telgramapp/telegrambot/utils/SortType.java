package telgramapp.telegrambot.utils;

import lombok.Getter;
import org.springframework.util.StringUtils;

import java.io.Serializable;

public enum SortType implements Serializable {
    asc("asc"),
    desc("desc");

    @Getter
    private String name;

    SortType(String name) {
        this.name = name;
    }

    public static SortType get(String name) {
        if (!StringUtils.isEmpty(name)) {
            for (SortType sortType : SortType.values()) {
                if (name.equals(sortType.getName())) {
                    return sortType;
                }
            }
        }
        return SortType.asc;
    }
}