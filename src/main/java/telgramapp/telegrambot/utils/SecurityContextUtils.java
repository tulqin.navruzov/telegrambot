package telgramapp.telegrambot.utils;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import telgramapp.telegrambot.controller.dto.UserAuth;

public final class SecurityContextUtils {
    public static Long getUserId() {
        SecurityContext context = getContext();
        if (context == null) {
            return null;
        }
        Authentication authentication = context.getAuthentication();
        if (authentication == null || authentication.getDetails() == null) {
            return null;
        }
        if (authentication.getDetails() instanceof UserAuth) {
            return ((UserAuth) authentication.getDetails()).getUserId();
        }
        return null;
    }

    private static SecurityContext getContext() {
        SecurityContext context = SecurityContextHolder.getContext();
        if (context == null || context.getAuthentication() == null
                || !context.getAuthentication().isAuthenticated()) {
            return null;
        }
        return context;
    }
}
