package telgramapp.telegrambot.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import telgramapp.telegrambot.db.EdsMember;

public interface MemberRepository extends JpaRepository<EdsMember, Long>, MemberRepositoryCustom {
}
