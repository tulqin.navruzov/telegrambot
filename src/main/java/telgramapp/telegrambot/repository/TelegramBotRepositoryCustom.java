package telgramapp.telegrambot.repository;

import telgramapp.telegrambot.db.EdsMember;
import telgramapp.telegrambot.db.EdsTelegramUser;
import telgramapp.telegrambot.utils.BaseListFilter;

import java.util.List;

public interface TelegramBotRepositoryCustom {

    Long getListCount(String searchKey);

    List<EdsTelegramUser> getList(BaseListFilter filter);

    EdsTelegramUser getTelegramUser(Long chatId);

    String getAnswerMessage(String key);
}
