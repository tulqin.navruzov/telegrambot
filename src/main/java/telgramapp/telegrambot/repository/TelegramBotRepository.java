package telgramapp.telegrambot.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import telgramapp.telegrambot.db.EdsTelegramUser;

public interface TelegramBotRepository extends JpaRepository<EdsTelegramUser, Long>, TelegramBotRepositoryCustom {
}
