package telgramapp.telegrambot.repository;

import telgramapp.telegrambot.db.EdsMember;
import telgramapp.telegrambot.utils.BaseListFilter;

import java.util.List;

public interface MemberRepositoryCustom {

    Long getListCount(String searchKey);

    List<EdsMember> getList(BaseListFilter filter);
}
