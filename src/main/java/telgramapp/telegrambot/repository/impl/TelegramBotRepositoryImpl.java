package telgramapp.telegrambot.repository.impl;

import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;
import telgramapp.telegrambot.db.EdsTelegramUser;
import telgramapp.telegrambot.repository.TelegramBotRepositoryCustom;
import telgramapp.telegrambot.utils.BaseListFilter;
import telgramapp.telegrambot.utils.SortType;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;

@Repository
public class TelegramBotRepositoryImpl implements TelegramBotRepositoryCustom {

    @PersistenceContext
    private EntityManager entityManager;


    @Override
    public Long getListCount(String searchKey) {
        final boolean hasSearch = !StringUtils.isEmpty(searchKey);
        String sql = "select count(m.id) from EdsTelegramUser m " +
                "  where 1=1  ";

        if (hasSearch) {
            sql += "  and (lower(m.firstName) like (:searchKey) " +
                    "    or lower(m.lastName) like (:searchKey) " +
                    "    or lower(m.userName) like (:searchKey) " +
                    "    or lower(m.phoneNumber) like (:searchKey)) ";
        }
        final TypedQuery<Long> query = this.entityManager.createQuery(sql, Long.class)
                .setMaxResults(1);
        if (hasSearch) {
            query.setParameter("searchKey", "%" + searchKey.toLowerCase() + "%");
        }
        List<Long> list = query.getResultList();
        return list.isEmpty() ? 0L : list.get(0);
    }

    @Override
    public List<EdsTelegramUser> getList(BaseListFilter filter) {
        boolean hasSearch = !StringUtils.isEmpty(filter.getSearchKey());
        boolean hasSort = !StringUtils.isEmpty(filter.getSortField());
        String sql = "select m from EdsTelegramUser m " +
                "  where 1=1  ";
        if (hasSearch) {
            sql += "  and (lower(m.firstName) like (:searchKey) " +
                    "    or lower(m.lastName) like (:searchKey) " +
                    "    or lower(m.userName) like (:searchKey) " +
                    "    or lower(m.phoneNumber) like (:searchKey)) ";
        }
        if (hasSort) {
            switch (filter.getSortField()) {
                case "firstName":
                    sql += "  order by m.firstName ";
                    break;
                case "lastName":
                    sql += "  order by m.lastName ";
                    break;
                case "email":
                    sql += "  order by m.userName ";
                    break;
                case "phone":
                    sql += "  order by m.phoneNumber ";
                    break;
                default:
                    sql += "  order by m.id ";
                    break;
            }
            sql += SortType.asc == filter.getSortOrder() ? " asc " : " desc ";
        } else {
            sql += " order by m.id desc ";
        }
        TypedQuery<EdsTelegramUser> query = this.entityManager.createQuery(sql, EdsTelegramUser.class)
                .setMaxResults(filter.getLimit())
                .setFirstResult(filter.getStart());
        if (hasSearch) {
            query.setParameter("searchKey", filter.getLikeSearchKey());
        }
        return query.getResultList();
    }

    @Override
    public EdsTelegramUser getTelegramUser(Long chatId) {
        String sql = "select tgu from EdsTelegramUser tgu " +
                "  where tgu.botBlocked <> true " +
                "  and tgu.chatId = " + chatId;

        final TypedQuery<EdsTelegramUser> query = this.entityManager.createQuery(sql, EdsTelegramUser.class)
                .setMaxResults(1);
        return query.getResultList().size() == 0 ? null : query.getResultList().get(0);
    }

    @Override
    public String getAnswerMessage(String key) {
        String sql = "select m.answerText from EdsAnswerMessage m " +
                "  where m.messageKey = '" + key.replace("'", "''") + "'";

        final TypedQuery<String> query = this.entityManager.createQuery(sql, String.class)
                .setMaxResults(1);
        return query.getResultList().size() == 0 ? "" : query.getResultList().get(0);

    }
}
