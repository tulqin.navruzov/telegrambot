package telgramapp.telegrambot.repository.impl;

import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;
import telgramapp.telegrambot.db.EdsMember;
import telgramapp.telegrambot.repository.MemberRepositoryCustom;
import telgramapp.telegrambot.utils.BaseListFilter;
import telgramapp.telegrambot.utils.SortType;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;

@Repository
public class MemberRepositoryImpl implements MemberRepositoryCustom {
    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public Long getListCount(String searchKey) {
        final boolean hasSearch = !StringUtils.isEmpty(searchKey);
        String sql = "select count(m.id) from EdsMember m " +
                "  where m.deleted = false ";

        if (hasSearch) {
            sql += "  and (lower(m.firstName) like (:searchKey) " +
                    "    or lower(m.lastName) like (:searchKey) " +
                    "    or lower(m.email) like (:searchKey) " +
                    "    or lower(m.phone) like (:searchKey)) ";
        }
        final TypedQuery<Long> query = this.entityManager.createQuery(sql, Long.class)
                .setMaxResults(1);
        if (hasSearch) {
            query.setParameter("searchKey", "%" + searchKey.toLowerCase() + "%");
        }
        List<Long> list = query.getResultList();
        return list.isEmpty() ? 0L : list.get(0);
    }

    @Override
    public List<EdsMember> getList(BaseListFilter filter) {
        boolean hasSearch = !StringUtils.isEmpty(filter.getSearchKey());
        boolean hasSort = !StringUtils.isEmpty(filter.getSortField());
        String sql = "select m from EdsMember m " +
                "  where m.deleted = false ";
        if (hasSearch) {
            sql += "  and (lower(m.firstName) like (:searchKey) " +
                    "    or lower(m.lastName) like (:searchKey) " +
                    "    or lower(m.email) like (:searchKey) " +
                    "    or lower(m.phone) like (:searchKey)) ";
        }
        if (hasSort) {
            switch (filter.getSortField()) {
                case "firstName":
                    sql += "  order by m.firstName ";
                    break;
                case "lastName":
                    sql += "  order by m.lastName ";
                    break;
                case "email":
                    sql += "  order by m.email ";
                    break;
                case "phone":
                    sql += "  order by m.phone ";
                    break;
                case "roleCode":
                    sql += "  order by m.roleCode ";
                    break;
                default:
                    sql += "  order by m.id ";
                    break;
            }
            sql += SortType.asc == filter.getSortOrder() ? " asc " : " desc ";
        } else {
            sql += " order by m.id desc ";
        }
        TypedQuery<EdsMember> query = this.entityManager.createQuery(sql, EdsMember.class)
                .setMaxResults(filter.getLimit())
                .setFirstResult(filter.getStart());
        if (hasSearch) {
            query.setParameter("searchKey", filter.getLikeSearchKey());
        }
        return query.getResultList();
    }
}
