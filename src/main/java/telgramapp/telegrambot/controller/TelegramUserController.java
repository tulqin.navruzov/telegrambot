package telgramapp.telegrambot.controller;

import lombok.AllArgsConstructor;
import lombok.NonNull;
import lombok.experimental.FieldDefaults;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import telgramapp.telegrambot.contants.ApiURLs;
import telgramapp.telegrambot.controller.dto.ListResult;
import telgramapp.telegrambot.controller.dto.ResponseData;
import telgramapp.telegrambot.controller.dto.TelegramUserTO;
import telgramapp.telegrambot.service.TelegramBotService;
import telgramapp.telegrambot.utils.BaseListFilter;

import static lombok.AccessLevel.PACKAGE;
import static lombok.AccessLevel.PRIVATE;

@RestController
@FieldDefaults(level = PRIVATE, makeFinal = true)
@AllArgsConstructor(access = PACKAGE)
@RequestMapping(ApiURLs.RootApi + ApiURLs.TelegramUsers)
public class TelegramUserController extends BaseController {

    @NonNull
    TelegramBotService telegramBotService;


    @GetMapping(
            value = list,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<ResponseData<ListResult<TelegramUserTO>>> getList(@ModelAttribute BaseListFilter filter) {
        return this.success(telegramBotService.getList(filter));
    }
}
