package telgramapp.telegrambot.controller;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import telgramapp.telegrambot.contants.ApiURLs;
import telgramapp.telegrambot.controller.dto.ResponseData;

public abstract class BaseController implements ApiURLs {


    <T> ResponseEntity<ResponseData<T>> success(T data) {
        return this.send(data, null, HttpStatus.OK);
    }

    <T> ResponseEntity<ResponseData<T>> success(String message) {
        return this.send(null, message, HttpStatus.OK);
    }

    private <T> ResponseEntity<ResponseData<T>> send(T data, String message, HttpStatus statusCode) {
        return new ResponseEntity<>(new ResponseData<>(data, message, statusCode.value()), getHttpHeaders(), HttpStatus.OK);
    }

    private HttpHeaders getHttpHeaders() {
        HttpHeaders h = new HttpHeaders();
        h.add("Content-Type", "application/json; charset=utf-8");
        h.add("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE");
        h.add("Access-Control-Max-Age", "3600");
        h.add("Access-Control-Allow-Headers", "Content-Type, Accept, X-Requested-With, remember-me");
        return h;
    }

    protected ResponseEntity<ResponseData> getResponse(HttpStatus httpStatus, String message) {
        ResponseData<Object> item = new ResponseData<>();
        item.setMessage(message);
        item.setStatus(httpStatus.value());
        return ResponseEntity.ok(item);
    }

}

