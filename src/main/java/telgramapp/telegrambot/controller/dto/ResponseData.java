package telgramapp.telegrambot.controller.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ResponseData<T> implements Serializable {
    private T data;
    private String message;
    private int status;

    public ResponseData(T data) {
        this(data, "Success", 200);
    }
}
