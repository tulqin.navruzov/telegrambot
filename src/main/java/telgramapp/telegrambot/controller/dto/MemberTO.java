package telgramapp.telegrambot.controller.dto;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class MemberTO implements Serializable {
    private Long id;
    private String fullName;
    private String firstName;
    private String middleName;
    private String lastName;
    private String email;
    private String phone;
    private String roleCode;
    private String login;
    private String password;
    private String note;
}