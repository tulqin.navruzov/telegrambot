package telgramapp.telegrambot.controller.dto;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.io.Serializable;

@Getter
@Setter
public class TelegramUserTO implements Serializable {
    private Long id;
    private Long chatId;
    private String userName;
    private String firstName;
    private String lastName;
    private String phoneNumber;
    private String location;
    private String lastMessage;
    private String uniqueCode;
    private Boolean botBlocked;
}