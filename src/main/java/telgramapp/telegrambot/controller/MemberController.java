package telgramapp.telegrambot.controller;

import lombok.AllArgsConstructor;
import lombok.NonNull;
import lombok.experimental.FieldDefaults;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import telgramapp.telegrambot.congfig.TokenAuthenticationHandler;
import telgramapp.telegrambot.contants.ApiURLs;
import telgramapp.telegrambot.controller.dto.MemberTO;
import telgramapp.telegrambot.controller.dto.ResponseData;
import telgramapp.telegrambot.service.MemberService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static lombok.AccessLevel.PACKAGE;
import static lombok.AccessLevel.PRIVATE;

@RestController
@FieldDefaults(level = PRIVATE, makeFinal = true)
@AllArgsConstructor(access = PACKAGE)
@RequestMapping(ApiURLs.RootApi + ApiURLs.Member)
public class MemberController extends BaseController {
    @NonNull
    TokenAuthenticationHandler tokenAuthenticationHandler;

    @NonNull
    MemberService memberService;

    @GetMapping(
            value = FetchMe,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<ResponseData<MemberTO>> fetchMe() {
        return this.success(memberService.fetchMe());
    }

    @GetMapping(
            value = Logout,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<ResponseData<String>> logout(HttpServletRequest request, HttpServletResponse response) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();

        if (auth != null) {
            new SecurityContextLogoutHandler().logout(request, response, auth);
            tokenAuthenticationHandler.removeAuthentication(request);
        }

        return success("Success");
    }

}
