package telgramapp.telegrambot.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.telegram.telegrambots.meta.api.objects.Update;
import telgramapp.telegrambot.service.TelegramBotService;

import javax.validation.Valid;

@RestController
public class HomeController {

    @Autowired
    TelegramBotService telegramBotService;

    @RequestMapping("/")
    public String home(@Valid @RequestBody Update update) {
        telegramBotService.setWebHook(update);
        return "Telegram bot application";
    }
}
