package telgramapp.telegrambot.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import telgramapp.telegrambot.contants.Constants;
import telgramapp.telegrambot.controller.dto.ResponseData;
import telgramapp.telegrambot.service.TelegramBotService;

import javax.validation.Valid;

@Slf4j
@RestController
@RequestMapping("/api/bot")
public class TelegramWebHookController extends BaseController {

    @Autowired
    TelegramBotService telegramBotService;

    @PostMapping("/" + Constants.LOCAL_BOT_TOKEN + "/web_hook")
    public ResponseEntity<ResponseData<String>> setWebHook(@Valid @RequestBody String response) {
        log.info("Telegram web hook request");
        return success("Telegram web hook request");
//        Update update = new Update();
//        telegramBotService.executeCommands(update);
    }

}
