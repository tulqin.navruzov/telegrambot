package telgramapp.telegrambot.contants;

public interface ApiURLs {
    String Version = "/v1";

    String WebApi = "/api";

    String RootApi = WebApi + Version;

    String Member = "/member";
    String TelegramUsers = "/telegramUsers";

    String FetchMe = "/fetch-me";

    String Logout = "/logout";

    String list = "/list";

    String Delete = "/item/{id}";

    String Item = "/item";


}
