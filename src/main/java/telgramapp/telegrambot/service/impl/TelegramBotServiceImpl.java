package telgramapp.telegrambot.service.impl;

import com.google.common.collect.Lists;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;
import telgramapp.telegrambot.botsender.TelegramBotSender;
import telgramapp.telegrambot.controller.dto.ListResult;
import telgramapp.telegrambot.controller.dto.TelegramUserTO;
import telgramapp.telegrambot.db.EdsTelegramUser;
import telgramapp.telegrambot.repository.TelegramBotRepository;
import telgramapp.telegrambot.service.TelegramBotService;
import telgramapp.telegrambot.utils.BaseListFilter;
import telgramapp.telegrambot.utils.DataGetter;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Slf4j
@Service
public class TelegramBotServiceImpl implements TelegramBotService {
    @Autowired
    private TelegramBotSender botSender;
    @Autowired
    private TelegramBotRepository telegramBotRepository;

    @Override
    public void setWebHook(Update update) {

        Message msg = update.getMessage();

        EdsTelegramUser tgUser = methodForStartAction(msg);
        boolean hasPhone = tgUser.getPhoneNumber() != null;
        String mText = tgUser.getLastMessage();
        if (msg.getText() != null) {
            mText = msg.getText();
        }
        String answerText;
        switch (mText) {
            case "/start":
            case "Bosh sahifa":
                answerText = telegramBotRepository.getAnswerMessage("/start");
                this.botSender.sendPhoto(update, answerText, hasPhone);
                break;
            case "Tanlov shartlari": {
                answerText = telegramBotRepository.getAnswerMessage(mText);
                this.botSender.sendMessage(update, answerText, "Ishtirok Etish", hasPhone);
                break;
            }
            case "Ishtirok Etish": {
                answerText = telegramBotRepository.getAnswerMessage(mText);
                this.botSender.sendMessage(update, answerText, "Bajarildi", hasPhone);
                break;
            }
            case "Bajarildi": {
                if (hasPhone) {
                    answerText = "Sizning o'yinda ishtirok etish codingiz " + tgUser.getUniqueCode() + "\n " +
                            " Konkurs tugaguniga qadar kanallardan chiqib ketish mutlaqo man etiladi, " +
                            " chiqib ketgan odam sovrinda qatnashmaydi. ";
                } else {
                    answerText = telegramBotRepository.getAnswerMessage("Bajarilmadi");
                }
                this.botSender.sendMessage(update, answerText, "Bosh sahifa", hasPhone);
                break;
            }
            default: {
                answerText = telegramBotRepository.getAnswerMessage(tgUser.getLastMessage());
                this.botSender.sendMessage(update, answerText, "Bosh sahifa", hasPhone);
                break;
            }
        }
    }

    private EdsTelegramUser methodForStartAction(Message msg) {

        EdsTelegramUser telegramUser = telegramBotRepository.getTelegramUser(msg.getChatId());
        String uniqueID = UUID.randomUUID().toString();
        if (telegramUser == null) {
            telegramUser = new EdsTelegramUser();
            telegramUser.setChatId(msg.getChatId());
            telegramUser.setUserName(msg.getFrom().getUserName());
            telegramUser.setFirstName(msg.getFrom().getFirstName());
            telegramUser.setLastName(msg.getFrom().getLastName());
        }
        telegramUser.setBotBlocked(false);
        if (msg.getText() != null) {
            String commandMessage = "Bosh sahifa".equals(msg.getText()) ? "/start" : msg.getText();
            telegramUser.setLastMessage(commandMessage);
        }
        if (telegramUser.getUniqueCode() == null) {
            telegramUser.setUniqueCode(uniqueID);

        }
        if (msg.getContact() != null) {
            telegramUser.setPhoneNumber(msg.getContact().getPhoneNumber());

        }
        telegramBotRepository.save(telegramUser);
        return telegramUser;
    }


    @Override
    @Transactional(readOnly = true)
    public ListResult<TelegramUserTO> getList(BaseListFilter filter) {
        List<TelegramUserTO> list = Lists.newArrayList();

        Long count = telegramBotRepository.getListCount(filter.getSearchKey());
        if (DataGetter.getLong(count) > 0) {
            List<EdsTelegramUser> domains = telegramBotRepository.getList(filter);
            list.addAll(domains.stream().map(EdsTelegramUser::toTO).collect(Collectors.toList()));
        }
        return new ListResult<>(list, count);
    }

}
