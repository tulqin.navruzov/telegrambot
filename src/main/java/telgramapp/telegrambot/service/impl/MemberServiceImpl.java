package telgramapp.telegrambot.service.impl;

import com.google.common.collect.Lists;
import lombok.AllArgsConstructor;
import lombok.NonNull;
import lombok.experimental.FieldDefaults;
import org.omg.CORBA.BAD_PARAM;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import telgramapp.telegrambot.controller.dto.ListResult;
import telgramapp.telegrambot.controller.dto.MemberTO;
import telgramapp.telegrambot.controller.dto.UserAuth;
import telgramapp.telegrambot.db.EdsMember;
import telgramapp.telegrambot.repository.MemberRepository;
import telgramapp.telegrambot.service.MemberService;
import telgramapp.telegrambot.utils.BaseListFilter;
import telgramapp.telegrambot.utils.DataGetter;
import telgramapp.telegrambot.utils.SecurityContextUtils;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static lombok.AccessLevel.PACKAGE;
import static lombok.AccessLevel.PRIVATE;

@Service
@FieldDefaults(level = PRIVATE, makeFinal = true)
@AllArgsConstructor(access = PACKAGE)
public class MemberServiceImpl implements MemberService {

    @NonNull
    MemberRepository memberRepository;
    
    @Override
    @Transactional(readOnly = true)
    public UserAuth getUserAuth(Long memberId) {
        if (memberId == null) {
            throw new RuntimeException("Session Expired");
        }
        Optional<EdsMember> EdsMember = memberRepository.findById(memberId);

        if (!EdsMember.isPresent()) {
            throw new RuntimeException("Session Expired");
        }
        EdsMember member = EdsMember.get();
        UserAuth userAuth = new UserAuth();
        userAuth.setUserId(member.getId());
        userAuth.setFirstName(member.getFirstName());
        userAuth.setLastName(member.getLastName());
        userAuth.setPhone(member.getPhone());
        userAuth.setEmail(member.getEmail());
        userAuth.setUsername(member.getLogin());
        userAuth.setPassword(member.getPassword());

        return userAuth;
    }

    @Override
    @Transactional(readOnly = true)
    public EdsMember getCurrentUser() {
        Long userId = SecurityContextUtils.getUserId();
        if (userId == null) {
            return null;
        }
        Optional<EdsMember> domain = memberRepository.findById(userId);
        if (!domain.isPresent() || domain.get().isDeleted()) {
            return null;
        }
        return domain.get();
    }

    @Override
    @Transactional(readOnly = true)
    public MemberTO fetchMe() {
        EdsMember domain = this.getCurrentUser();

        if (domain == null) {
            throw new RuntimeException("Session Expired");
        }
        return domain.shortTO();
    }

    @Override
    @Transactional(readOnly = true)
    public ListResult<MemberTO> getList(BaseListFilter filter) {
        List<MemberTO> list = Lists.newArrayList();

        Long count = memberRepository.getListCount(filter.getSearchKey());
        if (DataGetter.getLong(count) > 0) {
            List<EdsMember> domains = memberRepository.getList(filter);
            list.addAll(domains.stream().map(EdsMember::toTO).collect(Collectors.toList()));
        }
        return new ListResult<>(list, count);
    }

    @Override
    @Transactional(readOnly = true)
    public MemberTO getItem(Long id) {
        if (id == null) {
            throw new BAD_PARAM("Incorrect Params");
        }
        Optional<EdsMember> domain = memberRepository.findById(id);
        if (!domain.isPresent() || domain.get().isDeleted()) {
            throw new EntityNotFoundException("Entity Not Found");
        }
        return domain.map(EdsMember::toTO).orElse(null);
    }

    @Override
    @Transactional
    public String saveItem(MemberTO item, boolean isNew) {
        if (item == null
                || (item.getId() != null && isNew)
                || (item.getId() == null && !isNew)) {
            throw new BAD_PARAM("Incorrect Params");
        }
        EdsMember domain = null;
        if (item.getId() != null) {
            Optional<EdsMember> optional = memberRepository.findById(item.getId());
            domain = optional.orElse(null);
        }
        if (domain == null) {
            domain = new EdsMember();
        }
        domain.fromTO(item);
        memberRepository.save(domain);

        return "Successfully saved";
    }

    @Override
    @Transactional
    public String deleteItem(Long id) {
        if (id == null) {
            throw new BAD_PARAM("Id required ");
        }
        Optional<EdsMember> optional = memberRepository.findById(id);
        if (!optional.isPresent()) {
            throw new EntityNotFoundException("Entity Not Found");
        }
        optional.get().setDeleted(true);
        this.memberRepository.save(optional.get());

        return "Successfully deleted";
    }
}
