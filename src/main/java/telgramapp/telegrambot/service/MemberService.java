package telgramapp.telegrambot.service;

import telgramapp.telegrambot.controller.dto.ListResult;
import telgramapp.telegrambot.controller.dto.MemberTO;
import telgramapp.telegrambot.controller.dto.UserAuth;
import telgramapp.telegrambot.db.EdsMember;
import telgramapp.telegrambot.utils.BaseListFilter;

public interface MemberService {
    UserAuth getUserAuth(Long memberId);

    EdsMember getCurrentUser();

    MemberTO fetchMe();

    ListResult<MemberTO> getList(BaseListFilter filter);

    MemberTO getItem(Long id);

    String saveItem(MemberTO item, boolean isNew);

    String deleteItem(Long id);
}
