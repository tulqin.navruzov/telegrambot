package telgramapp.telegrambot.service;

import org.telegram.telegrambots.meta.api.objects.Update;
import telgramapp.telegrambot.controller.dto.ListResult;
import telgramapp.telegrambot.controller.dto.TelegramUserTO;
import telgramapp.telegrambot.utils.BaseListFilter;

public interface TelegramBotService {

    void setWebHook(Update update);

    ListResult<TelegramUserTO> getList(BaseListFilter filter);

}
