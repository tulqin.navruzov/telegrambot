package telgramapp.telegrambot.db;

import lombok.Getter;
import lombok.Setter;
import telgramapp.telegrambot.controller.dto.MemberTO;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Getter
@Setter
@Entity
@Table(name = "member", schema = "public")
public class EdsMember implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @NotNull
    @Column(nullable = false)
    private boolean deleted;

    private String firstName;
    private String middleName;
    private String lastName;
    private String phone;
    private String email;
    private String login;
    private String password;


    public String getFullName() {
        return this.getFirstName() + " " + this.getLastName();
    }

    public MemberTO toTO() {
        MemberTO to = new MemberTO();
        to.setId(this.getId());
        to.setFirstName(this.getFirstName());
        to.setMiddleName(this.getMiddleName());
        to.setLastName(this.getLastName());
        to.setEmail(this.getEmail());
        to.setPhone(this.getPhone());
        return to;
    }

    public MemberTO shortTO() {
        MemberTO to = new MemberTO();
        to.setId(this.getId());
        to.setFirstName(this.getFirstName());
        to.setLastName(this.getLastName());
        to.setEmail(this.getEmail());
        return to;
    }

    public void fromTO(MemberTO to) {
        this.setFirstName(to.getFirstName());
        this.setMiddleName(to.getMiddleName());
        this.setLastName(to.getLastName());
        this.setPhone(to.getPhone());
        this.setEmail(to.getEmail());
    }
}