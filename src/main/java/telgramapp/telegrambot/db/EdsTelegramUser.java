package telgramapp.telegrambot.db;

import lombok.Getter;
import lombok.Setter;
import telgramapp.telegrambot.controller.dto.TelegramUserTO;

import javax.persistence.*;
import java.io.Serializable;

@Getter
@Setter
@Entity
@Table(name = "telegramuser", schema = "public")
public class EdsTelegramUser implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private Long chatId;

    private String userName;
    private String firstName;
    private String lastName;
    private String phoneNumber;
    private String location;
    private String lastMessage;
    private String uniqueCode;
    private Boolean botBlocked;

    public TelegramUserTO toTO() {
        TelegramUserTO to = new TelegramUserTO();
        to.setId(this.getId());
        to.setChatId(this.getChatId());
        to.setFirstName(this.getFirstName());
        to.setLastName(this.getLastName());
        to.setPhoneNumber(this.getPhoneNumber());
        to.setUserName(this.getUserName());
        return to;
    }

}
